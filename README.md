# Ink Dbus

A test of the new dbus features of Inkscape 1.2.1+ 

This allows an Inkscape session to be controlled from a separate gui.

It supports running basic macros.

There are 3 branches of dbus actions: 

Application / Window / Document

Choose an action - add a parameter if required, click run to activate the action.

Double click the action to add to the macro list.

The items in the macro list can be toggled on / off or deleted.
The macro list supports drag reordering of items.
You can set a number of repeats - for example you could simplify 72 times if you wanted to.

For Ubuntu 20+ everything should work.

For Windows 10+

1. the path to Inkscape python is assumed to be c:/program files/inkscape/bin/python.exe'

2. The 'Window' branch of dbus does not work. I have raised an issue on Inkscape gitlab for this.

-------

Please note:

It does help if you have some knowledge of how the command line works.

Most of the Application dbus actions mirror the command line and its parameters.

Some of the commands act as 'flags' which are set, for example if you set `export-area-page` to True, then it has to be set to False again before you can use 
`export-area-drawing` set to True.
As a quirk `export-area` which takes dimensions, once used you cannot go back to `export-area-page` etc. for that session.

This is a new feature of Inkscape - so be prepared for exciting thinks to happen unexpectedly.
