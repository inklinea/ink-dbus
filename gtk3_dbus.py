#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Ink Dbus- a dbus based inkscape extension
# An Inkscape 1.2.1+ extension
##############################################################################


import inkex

# Kill all warning popups

# import warnings
# warnings.filterwarnings("ignore")

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

from ink_dbus import *

import sys

# -------------------------------------------------------------------------------
# HANDLERS
# -------------------------------------------------------------------------------

class Handler:
    def on_destroy(self, *args):
        Gtk.main_quit()

    def window_button_press(self, *args):
        inkex.errormsg(args[:])

    def macro_toggle_delete(self, *args):

        my_treeview = builder.get_object("macro_treeview")

        index = args[1]

        tree_selection = my_treeview.get_selection()

        tree_selection.select_path(index)

        model, paths = tree_selection.get_selected_rows()

        for path in paths:
            tree_iter = model.get_iter(path)
            model.remove(tree_iter)

    def get_file_chooser_path(self, *args):

        # Signal is the GtkDialog Response

        file_chooser_dialog = args[0]
        filename = file_chooser_dialog.get_filename()
        # inkex.errormsg(filename)
        # inkex.errormsg('filechooserclosed')
        if filename != None:

            # For safety, lets strip the file extension and replace
            # it with inkdbus

            from pathlib import Path
            filename_path = Path(filename)
            filename_extension = filename_path.suffix
            # inkex.errormsg(type(filename_extension))
            if filename_extension.strip() != '':
                filename = filename.replace(filename_extension, '.inkdbus')
            else:
                filename = filename + '.inkdbus'
            debug_textbuffer.set_text(filename)

            csv_file_chooser = builder.get_object("csv_file_chooser")

            # csv_filepath = csv_file_chooser.set_filename(filename)
            output_file_label = builder.get_object('output_file_label')
            output_file_label.set_label(filename)
        else:
            None
            # inkex.errormsg('filename is none')

    def send_click_to_filechooserbutton(self, *args):
        debug_textbuffer.set_text('click')

        csv_file_chooser = builder.get_object("filechooserdialog1")

        csv_file_chooser.show()

    def run_action_button(self, *args):

        button_name = args[0].get_name().split('_')[0]

        dbus_path = button_name

        treeview = builder.get_object(f'{dbus_path}_treeview')

        tree_selection = treeview.get_selection()

        model, treeiter = tree_selection.get_selected()

        if treeiter is not None:
            row_index = model.get_path(treeiter)
        else:
            return

        actions_liststore = builder.get_object(f'{dbus_path}_actions_liststore')

        action = actions_liststore[row_index][0].strip()
        param = actions_liststore[row_index][1].strip()
        param_type = actions_liststore[row_index][5].strip()

        if action is not None:
            InkDbus.ink_dbus_action(None, dbus_path, action, param, param_type, None)

    def print_liststore(self, *args):
        macro_liststore = builder.get_object("macro_liststore")
        for row in macro_liststore:
            for cell in row:
                inkex.errormsg(cell)

    def liststore_to_csv(self, *args):

        csv_filepath = builder.get_object("output_file_label").get_text()

        if csv_filepath == 'No File Selected':
            debug_textbuffer.set_text('Please Select A Filename')
            return

        import csv

        try:
            with open(csv_filepath, "w", newline='') as macro_csv:
                csvwriter = csv.writer(macro_csv, delimiter='|')

                macro_liststore = builder.get_object("macro_liststore")
                csvwriter.writerow(['macro_csv'])
                for row in macro_liststore:
                    csvwriter.writerow(row)
            debug_textbuffer.set_text('Save Successful')
        except:
            debug_textbuffer.set_text('Invalid Path')

    def csv_to_liststore(self, *args):

        csv_filepath = builder.get_object("output_file_label").get_text()

        if csv_filepath == 'No File Selected':
            debug_textbuffer.set_text('Please Select A File')
            return


        if csv_filepath == None:
            # global debug_textbuffer
            debug_textbuffer.set_text('No Macro Data Found')
            return
        else:
            # global debug_textbuffer
            debug_textbuffer.set_text('')

        import csv
        with open(csv_filepath, "r", newline='') as macro_csv:
            first_line = macro_csv.readline()

            if first_line.strip() != 'macro_csv':
                return
            # File reader has now moved to 2nd row
            rows = macro_csv.readlines()

            reader = csv.reader(rows, delimiter='|')

            macro_liststore = builder.get_object("macro_liststore")
            macro_liststore.clear()

            for row in reader:
                # csv returns text, but liststore is type sensitive
                macro_liststore.append([row[0], row[1], int(row[2]), int(row[3]), row[4], int(row[5]), row[6], row[7]])

    def clear_macro_liststore(self, *args):
        macro_liststore = builder.get_object("macro_liststore")
        macro_liststore.clear()

    def action_treeview_parameter_cell_edited(self, *args):

        row_index = args[1]

        cell_contents = args[2]
        cell_renderer_name = args[0].name
        dbus_type = cell_renderer_name.split('_')[0]

        # Set the liststore to match the edited values
        # Otherwise cell will just revert to original liststore value
        actions_liststore = builder.get_object(f'{dbus_type}_actions_liststore')

        actions_liststore[row_index][1] = cell_contents


    def macro_treeview_cell_edited(self, *args):

        row_index = args[1]
        cell_contents = args[2]

        # Set the liststore to match the edited values
        # Otherwise cell will just revert to original liststore value
        macro_liststore = builder.get_object("macro_liststore")

        macro_liststore[row_index][1] = cell_contents


    def record_mousebutton(self, *args):
        # inkex.errormsg(args[:])
        mouse_press_event = args[1]
        # inkex.errormsg(mouse_press_event.button)
        global mousebutton
        mousebutton = mouse_press_event.button
        # inkex.errormsg(mousebutton)

    def query_button(self, *args):
        import time
        sys.stdout.write(str(time.time()))
        # print('print hellloo')

    def row_activated(self, *args):

        # can we get the widget name ?
        # So we know if application/window/document action ? 
        treename = args[0].get_name().split('_')[0]

        row_index = args[1].get_indices()

        macro_liststore = builder.get_object("macro_liststore")
        actions_liststore = builder.get_object(f'{treename}_actions_liststore')

        action = actions_liststore[row_index]

        macro_liststore.append([action[0], action[1], 1, 1, action[4], 1, action[5], action[6]])


    def on_macro_cell_toggled(self, widget, path):

        macro_liststore = builder.get_object("macro_liststore")

        macro_liststore[path][3] = not macro_liststore[path][3]

    def on_amount_edited(self, widget, path, value):
        macro_liststore = builder.get_object("macro_liststore")
        macro_liststore[path][2] = int(value)

    def macro_row_delete(self, widget, path):
        inkex.errormsg('clicked')


    def run_macro_button(self, *args):
        macro_liststore = builder.get_object("macro_liststore")
        for row in macro_liststore:

            action = row[0].strip()
            param = row[1].strip()
            param_type = row[6].strip()
            dbus_path = row[4].strip()

            repeat = row[2]
            toggle = row[3]

            if toggle == 1:
                # inkex.errormsg(f'Repeat {repeat}')
                while repeat > 0:
                    # inkex.errormsg('pos tog')
                    InkDbus.ink_dbus_action(None, dbus_path, action, param, param_type, None)
                    repeat -= 1
# -------------------------------------------------------------------------------
# END OF HANDLERS
# -------------------------------------------------------------------------------


# -------------------------------------------------------------------------------
# MAIN GTK3 LOOP
# -------------------------------------------------------------------------------

def run_gtk():
    # Load stylesheet
    screen = Gdk.Screen.get_default()
    provider = Gtk.CssProvider()
    provider.load_from_path("gtk3_dbus.css")
    Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    global builder
    builder = Gtk.Builder()

    builder.add_from_file('gtk3_dbus.glade')

    global window
    window = builder.get_object("main_window")
    builder.connect_signals(Handler())
    window.connect("destroy", Gtk.main_quit)
    window.show_all()

    global mousebutton
    mousebutton = 'left'


    import csv

    # Populate action_liststore

    application_actions_liststore = builder.get_object("application_actions_liststore")

    application_actions_list = ['none', '', 1, 1, 'application', 'param_type', 'tooltip']

    with open("application_dbus_gtk3.csv", "r", newline='') as macro_csv:
        first_line = macro_csv.readline()

        # File reader has now moved to 2nd row
        rows = macro_csv.readlines()
        # inkex.errormsg(rows)
        reader = csv.reader(rows, delimiter='|')

        for row in reader:

            application_actions_list[0] = row[0]
            application_actions_list[1] = row[1]
            application_actions_list[5] = row[2]
            application_actions_list[-1] = row[3]
            application_actions_liststore.append(application_actions_list)

    # Add a name to the cell render so we know which treeview it
    # belongs to
    application_param_cellrenderer = builder.get_object("application_param_cellrenderer")
    application_param_cellrenderer.name = 'application_param_cellrenderer'

    # Populate window_liststore

    window_actions_liststore = builder.get_object("window_actions_liststore")

    window_list = ['none', '', 1, 1, 'window', 'param_type', 'tooltip']

    with open("window_dbus_gtk3.csv", "r", newline='') as macro_csv:
        first_line = macro_csv.readline()

        # File reader has now moved to 2nd row
        rows = macro_csv.readlines()
        # inkex.errormsg(rows)
        reader = csv.reader(rows, delimiter='|')

        for row in reader:

            window_list[0] = row[0]
            window_list[1] = row[1]
            window_list[5] = row[2]
            window_list[-1] = row[3]
            window_actions_liststore.append(window_list)

        # Add a name to the cell render so we know which treeview it
        # belongs to
        window_param_cellrenderer = builder.get_object("window_param_cellrenderer")
        window_param_cellrenderer.name = 'window_param_cellrenderer'

    # Populate document_liststore

    document_actions_liststore = builder.get_object("document_actions_liststore")

    document_list = ['none', '', 1, 1, 'document', 'param_type', 'tooltip']

    with open("document_dbus_gtk3.csv", "r", newline='') as macro_csv:
        first_line = macro_csv.readline()
        # inkex.errormsg(first_line)

        # File reader has now moved to 2nd row
        rows = macro_csv.readlines()
        # inkex.errormsg(rows)
        reader = csv.reader(rows, delimiter='|')

        for row in reader:

            document_list[0] = row[0]
            document_list[1] = row[1]
            document_list[5] = row[2]
            document_list[-1] = row[3]
            document_actions_liststore.append(document_list)

        # Add a name to the cell render so we know which treeview it
        # belongs to
        document_param_cellrenderer = builder.get_object("document_param_cellrenderer")
        document_param_cellrenderer.name = 'document_param_cellrenderer'

    # sys.exit()
    global debug_textbuffer
    debug_textbuffer = builder.get_object("debug_textbuffer")
    debug_textbuffer.set_text('Ready !')

    # add save action to the general filechooser dialogue used by
    # file save butons
    file_chooser = builder.get_object("filechooserdialog1")
    # inkex.errormsg(Inklin.get_attributes(csv_file_chooser.set_action))
    # This is the save flag
    file_chooser_action = Gtk.FileChooserAction(1)
    file_chooser.set_action(file_chooser_action)

    Gtk.main()

# -------------------------------------------------------------------------------
# END OF MAIN GTK3 LOOP
# -------------------------------------------------------------------------------


class GtkInkDbus(inkex.EffectExtension):

    def effect(self):

        run_gtk()

# Lets check if this is to be a standalone window
if 'standalone' in sys.argv:

    run_gtk()

# sys.exit()

if __name__ == '__main__':
    GtkInkDbus().run()
